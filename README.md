rs-math_lang
=====

a math language

[functions](spec/functions.md)
[modules](spec/modules.md)
[sets](spec/sets.md)
[traits](spec/traits.md)
