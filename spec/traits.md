# Traits

```
trait Add(t) where
    output <- Self
    add = (Self, t) -> output

trait Sub(t) where
    output <- Self
    sub = (Self, t) -> output

trait Num(t) where
    Add(t, output = t)
    Sub(t, output = t)


impl Add(N) for N where
    output <- Self
    add = (a, b) -> a + b

impl Sub(N) for N where
    output <- Self
    sub = (a, b) -> a - b
```
