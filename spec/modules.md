# Modules


## src/string.ml
```
export string

string = "Hello, world!"
```


## src/utils/get_index.ml
```
import string from "../string"

export get_index

let get_index <- { T in Natural : (T) -> Char }
    get_index(i) = string[i]
```


## src/main.ml
```
import println from "std/io"
import get_index from "./utils/get_index"
import string from "./string"

export main


let main <- { () -> {} }
    main() = do
      println(string)       # "Hello, world!"
      println(get_index(0)) # 'H'
```
