# Sets

## Natural numbers

```
let N <- Z
    N = { 0, 1, ... Inf }
```

## Integer

```
let Z <- R
    Z = { x <- R : x mod 1 == 0 }
```

## Rational numbers

```
let Q <- R
    Q = { p, q <- Z : q != 0 and q mod p == 0 }
```

## Real numbers
```
let R <- C
    R = { -Inf...Inf }
```

## Complex numbers
```
let C = { -(Inf + i)...(Inf + i) }
```

# Other sets

## Set of all characters
```
let Char <- N
    Char = { '\0', '\1', ... }
```

## Set of all lists of the Set T
```
let List(t) <- { (t) }

let list_a = (0, 1, 2, 3)
list_a <- List(N) # true

let list_b = push(list_a, 4)
pop(list_b)
```

## Set of all tuples of the Set T
```
let Tuple(t) = { [t] }

let tuple_a = [0, 1, 2, 3]
tuple_a <- Tuple(N) # true

let tuple_b = push(tuple_a, 4)
tuple_b[0]
tuple_b.0
```

## Set of all lists of the Set Char
```
let String = Tuple(Char)
let LinkedString = List(Char)
```

## Custom data types
```
let Person = {
  name = String
  age = N
}
```
