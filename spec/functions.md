# Functions

```
let f = N -> Z
    f = x -> x - 4

# A <- B means A is a subset of B

{2, -2} <- f # true
{0, 0} <- f  # false

let add = (R, R) -> R
    add = (a, b) -> a + b

{(1, 2), 3} <- add # true
{(2, 2), 5} <- add # false

let create_inc = R -> {} -> R
    create_inc = a -> {} -> a + 1

{1, { {}, 2 }} <- create_inc # true
{2, { {}, 4 }} <- create_inc # false
```
